-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 11, 2018 at 02:00 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1-log
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mtdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors`
--

CREATE TABLE `actors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `sex` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_bio_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `awards` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imdb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` bigint(20) NOT NULL DEFAULT '1',
  `tmdb_id` bigint(20) UNSIGNED DEFAULT NULL,
  `fully_scraped` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `allow_update` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `actors_titles`
--

CREATE TABLE `actors_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `actor_id` bigint(20) UNSIGNED NOT NULL,
  `title_id` bigint(20) UNSIGNED NOT NULL,
  `char_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unknown',
  `known_for` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Generic Name',
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa fa-fire',
  `query` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `show_trailer` tinyint(1) NOT NULL DEFAULT '0',
  `show_rating` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `weight` int(11) NOT NULL DEFAULT '1',
  `limit` int(11) NOT NULL DEFAULT '8',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `update_from_external` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categorizables`
--

CREATE TABLE `categorizables` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `categorizable_id` bigint(20) UNSIGNED NOT NULL,
  `categorizable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'title',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allow_update` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `directors_titles`
--

CREATE TABLE `directors_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `director_id` bigint(20) UNSIGNED NOT NULL,
  `title_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `episodes`
--

CREATE TABLE `episodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plot` text COLLATE utf8_unicode_ci,
  `poster` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `release_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` bigint(20) UNSIGNED NOT NULL,
  `season_id` bigint(20) UNSIGNED NOT NULL,
  `season_number` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `episode_number` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `allow_update` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promo` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `group_activity`
--

CREATE TABLE `group_activity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `local` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'external'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'embed',
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` bigint(20) UNSIGNED DEFAULT NULL,
  `season` int(10) UNSIGNED DEFAULT NULL,
  `episode` int(10) UNSIGNED DEFAULT NULL,
  `reports` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `positive_votes` int(11) NOT NULL DEFAULT '0',
  `negative_votes` int(11) NOT NULL DEFAULT '0',
  `quality` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SD'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_09_01_106539_create_categorizables_table', 1),
('2013_12_06_201055_create_titles', 1),
('2013_12_07_105031_create_actors', 1),
('2013_12_07_105057_create_directors', 1),
('2013_12_07_105110_create_writers', 1),
('2013_12_07_105130_create_options', 1),
('2013_12_07_105216_create_actors_titles', 1),
('2013_12_07_105239_create_directors_titles', 1),
('2013_12_07_105257_create_writers_titles', 1),
('2013_12_07_105324_create_episodes', 1),
('2013_12_07_105349_create_group_activity', 1),
('2013_12_07_105349_create_user_activity', 1),
('2013_12_07_105409_create_images', 1),
('2013_12_07_105420_create_news', 1),
('2013_12_07_105432_create_reviews', 1),
('2013_12_07_105447_create_seasons', 1),
('2013_12_07_120615_create_users', 1),
('2013_12_07_120644_create_groups', 1),
('2013_12_07_120703_create_throttle', 1),
('2013_12_07_120720_create_users_groups', 1),
('2013_12_07_170342_create_users_titles', 1),
('2014_01_02_134303_add_columns_to_titles', 1),
('2014_01_02_211657_add_columns_to_news', 1),
('2014_01_02_211658_add_columns_to_users', 1),
('2014_01_12_135844_create_social', 1),
('2014_01_12_139511_make_options_text', 1),
('2014_01_14_132561_add_promo_to_episodes', 1),
('2014_01_14_142568_increate_gender_len', 1),
('2014_01_25_153483_increase_custom_field_len', 1),
('2014_05_08_169554_create_pages_table', 1),
('2014_06_15_131485_create_slides', 1),
('2014_06_16_148413_add_type_to_images_table', 1),
('2014_06_17_145413_add_type_to_reviews_table', 1),
('2014_06_21_138813_add_columns_to_slider_table', 1),
('2014_06_29_2101055_create_links', 1),
('2014_06_29_2101065_create_reports', 1),
('2014_08_30_138913_create_categories_table', 1),
('2014_09_02_132571_add_poster_to_slides_table', 1),
('2014_10_30_132541_add_indexes', 1),
('2015_01_14_132671_add_approved_to_links_table', 1),
('2015_07_24_132662_add_new_columns_to_links_table', 1),
('2015_10_25_136541_add_indexes_to_titles_table', 1),
('2016_01_29_136542_add_columns_to_categories_table', 1),
('2016_04_01_132542_add_indexes_to_links_table', 1),
('2017_03_20_136542_add_index_to_tmdb_popularity_field', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fully_scraped` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'logo', '../../../../../../logo.png', '2018-02-19 04:08:39', NULL),
(2, 'siteName', '  ', '2018-02-19 04:08:39', NULL),
(3, 'metaTitle', ' ', '2018-02-19 04:08:39', NULL),
(4, 'siteDescription', ' ', '2018-02-19 04:08:39', NULL),
(5, 'mainSiteKeywords', ' ', '2018-02-19 04:08:39', NULL),
(6, 'titlePageKeywords', ' ', '2018-02-19 04:08:39', NULL),
(7, 'actorPageKeywords', ' ', '2018-02-19 04:08:39', NULL),
(8, 'data_provider', 'db', '2018-02-19 04:08:39', NULL),
(9, 'search_provider', 'db', '2018-02-19 04:08:39', NULL),
(10, 'news_provider', 'imdb', '2018-02-19 04:08:39', NULL),
(11, 'index_3rd_party_data', '0', '2018-02-19 04:08:39', NULL),
(12, 'enable_news_scraping', '0', '2018-02-19 04:08:39', NULL),
(13, 'enable_reviews_scraping', '0', '2018-02-19 04:08:39', NULL),
(14, 'disable_reviews', '0', '2018-02-19 04:08:39', NULL),
(15, 'disable_news', '0', '2018-02-19 04:08:39', NULL),
(16, 'tmdb_api_key', '', '2018-02-19 04:08:39', NULL),
(17, 'youtube_api_key', '', '2018-02-19 04:08:39', NULL),
(18, 'disqus_short_name', '', '2018-02-19 04:08:39', NULL),
(19, 'contact_us_email', '', '2018-02-19 04:08:39', NULL),
(20, 'fb_url', '', '2018-02-19 04:08:39', NULL),
(21, 'google_url', '', '2018-02-19 04:08:39', NULL),
(22, 'tw_url', '', '2018-02-19 04:08:39', NULL),
(23, 'youtube_url', '', '2018-02-19 04:08:39', NULL),
(24, 'amazon_id', '', '2018-02-19 04:08:39', NULL),
(25, 'theme', 'original', '2018-02-19 04:08:39', NULL),
(26, 'autoplay_slider', '0', '2018-02-19 04:08:39', NULL),
(27, 'index_per_page', '36', '2018-02-19 04:08:39', NULL),
(28, 'index_default_order', 'release_dateDesc', '2018-02-19 04:08:39', NULL),
(29, 'enable_buy_now', '0', '2018-02-19 04:08:39', NULL),
(30, 'video_player', 'custom', '2018-02-19 04:08:39', NULL),
(31, 'enable_news', '0', '2018-02-19 04:08:39', NULL),
(32, 'tmdb_language', '', '2018-02-19 04:08:39', NULL),
(33, 'save_tmdb', '1', '2018-02-19 04:08:39', NULL),
(34, 'uri_separator', '', '2018-02-19 04:08:39', NULL),
(35, 'uri_case', 'lowercase', '2018-02-19 04:08:39', NULL),
(36, 'require_act', '1', '2018-02-19 04:08:39', NULL),
(37, 'genres', 'Action|Adventure|Animation|Biography|Comedy|Crime|Documentary|Drama|Family|Fantasy|History|Horror|Music|Musical|Mystery|Romance|Science Fiction|Thriller|War|Western', '2018-02-19 04:08:39', NULL),
(38, 'LinkOffer', 'https://tcground.com/', '2018-02-19 00:00:00', NULL),
(39, 'menus', '[{\"name\":\"Top Menu\",\"position\":\"header\",\"active\":\"1\",\"items\":[{\"label\":\"Home\",\"action\":\"..\\/..\\/..\\/..\\/..\\/..\\/\",\"weight\":\"1\",\"type\":\"link\",\"children\":[],\"visibility\":\"everyone\"},{\"label\":\"Movies\",\"action\":\"..\\/..\\/..\\/..\\/..\\/..\\/movies\",\"weight\":\"2\",\"type\":\"link\",\"children\":[],\"visibility\":\"everyone\"},{\"label\":\"Series\",\"action\":\"..\\/..\\/..\\/..\\/..\\/..\\/series\",\"weight\":\"3\",\"type\":\"link\",\"children\":[],\"visibility\":\"everyone\"},{\"label\":\"People\",\"action\":\"..\\/..\\/..\\/..\\/..\\/..\\/people\",\"weight\":\"4\",\"type\":\"link\",\"children\":[],\"visibility\":\"everyone\"}]},{\"name\":\"Menu Bottom\",\"position\":\"footer\",\"active\":\"footer\",\"items\":[]}]', '2018-02-19 05:38:08', '2018-03-09 00:00:00'),
(40, 'FirstTitle', ' ', '2018-03-06 05:27:44', NULL),
(41, 'LastTitle', ' ', '2018-03-06 05:27:44', NULL),
(42, 'KeywordUtama1', ' ', '2018-03-06 06:19:50', NULL),
(43, 'KeywordUtama2', ' ', '2018-03-06 06:19:50', NULL),
(44, 'KeywordUtama3', ' ', '2018-03-06 06:19:50', NULL),
(45, 'OptionYear', '0', '2018-03-06 06:27:12', NULL),
(46, 'Histat', ' ', '2018-03-11 13:59:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibility` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'public',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `link_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `score` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'critic'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seasons`
--

CREATE TABLE `seasons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `release_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poster` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `overview` text COLLATE utf8_unicode_ci,
  `number` int(11) NOT NULL DEFAULT '1',
  `title_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title_imdb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_tmdb_id` bigint(20) UNSIGNED DEFAULT NULL,
  `fully_scraped` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `allow_update` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `director` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stars` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trailer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trailer_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poster` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `title`, `body`, `link`, `image`, `genre`, `director`, `stars`, `trailer`, `trailer_image`, `poster`) VALUES
(1, 'Black Panther', 'T\'Challa, after the death of his father, the King of Wakanda, returns home to the isolated, technologically advanced African nation to succeed to the throne and take his rightful place as king.', 'http://mtdb.getyourmoney.top/movies/74-black-panther', 'https://image.tmdb.org/t/p/original/AlFqBwJnokrp9zWTXOUv7uhkaeq.jpg', 'Action |  Adventure |  Fantasy |  Science Fiction', 'Ryan Coogler', 'Chadwick Boseman, Michael B. Jordan, Lupita Nyong\'o', 'https://www.youtube.com/embed/xjDjIWPwcPU', 'https://image.tmdb.org/t/p/original/b6ZJZHUdMEFECvGiDpJjlfUWela.jpg', 'https://image.tmdb.org/t/p/w342/bLBUCtMQGJclH36clliPLmljMys.jpg'),
(5, 'Maze Runner: The Death Cure', 'Thomas leads his group of escaped Gladers on their final and most dangerous mission yet. To save their friends, they must break into the legendary Last City, a WCKD-controlled labyrinth that may turn out to be the deadliest maze of all. Anyone who makes it out alive will get answers to the questions the Gladers have been asking since they first arrived in the maze.', 'http://mtdb.getyourmoney.top/movies/80-maze-runner:-the-death-cure', 'https://image.tmdb.org/t/p/w780/bvbyidkMaBls1LTaIWYY6UmYTaL.jpg', 'Action |  Mystery |  Science Fiction |  Thriller', 'Wes Ball', 'Thomas Brodie-Sangster, Ki Hong Lee, Dylan O\'Brien', 'https://www.youtube.com/embed/4-BTxXm8KSg', 'https://image.tmdb.org/t/p/original/zgcL0xaHxXVvrV1Fu1ZSKoTk0MH.jpg', 'https://image.tmdb.org/t/p/w342/7GgZ6DGezkh3szFdvskH5XD4V0t.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE `social` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `service_user_identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'movie',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, NULL, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` enum('movie','series') COLLATE utf8_unicode_ci DEFAULT NULL,
  `imdb_rating` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tmdb_rating` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mc_user_score` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mc_critic_score` smallint(5) UNSIGNED DEFAULT NULL,
  `mc_num_of_votes` int(10) UNSIGNED DEFAULT NULL,
  `imdb_votes_num` bigint(20) UNSIGNED DEFAULT NULL,
  `release_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` smallint(5) UNSIGNED DEFAULT NULL,
  `plot` text COLLATE utf8_unicode_ci,
  `genre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `poster` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `awards` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `runtime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trailer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `budget` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `revenue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` bigint(20) NOT NULL DEFAULT '1',
  `tmdb_popularity` float(50,2) UNSIGNED DEFAULT NULL,
  `imdb_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tmdb_id` bigint(20) UNSIGNED DEFAULT NULL,
  `season_number` tinyint(3) UNSIGNED DEFAULT NULL,
  `fully_scraped` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `allow_update` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `now_playing` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `affiliate_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `gender`, `avatar`, `created_at`, `updated_at`, `background`) VALUES
(1, 'admin', 'example@gmail.com', '$2y$10$tyVNGwm0YEY2ncpjMRNzR.dHhZ9XxZq/mFdHkqLKLYLb9l23bPQVC', '{\"superuser\":1}', 1, NULL, NULL, '2018-03-11 15:58:03', '$2y$10$3otUYVN5dfXRV6uX2amk9u1umV35K4L07P.w1c/O1QvYna09gYbVO', NULL, '', '', 'male', 'avatars/1.jpg', '2018-02-19 05:40:52', '2018-03-11 15:58:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_titles`
--

CREATE TABLE `users_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title_id` int(10) UNSIGNED NOT NULL,
  `watchlist` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `favorite` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_titles`
--

INSERT INTO `users_titles` (`id`, `user_id`, `title_id`, `watchlist`, `favorite`, `created_at`, `updated_at`) VALUES
(1, 1, 73, 0, 1, '2018-02-19 06:42:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `writers`
--

CREATE TABLE `writers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `allow_update` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temp_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `writers_titles`
--

CREATE TABLE `writers_titles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `writer_id` bigint(20) UNSIGNED NOT NULL,
  `title_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `actors_name_unique` (`name`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `actors_temp_id_index` (`temp_id`);

--
-- Indexes for table `actors_titles`
--
ALTER TABLE `actors_titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `actor_title_unique` (`actor_id`,`title_id`),
  ADD KEY `actors_titles_actor_id_index` (`actor_id`),
  ADD KEY `actors_titles_title_id_index` (`title_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `categorizables`
--
ALTER TABLE `categorizables`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ccc_unique` (`category_id`,`categorizable_id`,`categorizable_type`),
  ADD KEY `categorizables_category_id_index` (`category_id`),
  ADD KEY `categorizables_categorizable_id_index` (`categorizable_id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `directors_name_unique` (`name`);

--
-- Indexes for table `directors_titles`
--
ALTER TABLE `directors_titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `director_title_unique` (`director_id`,`title_id`),
  ADD KEY `directors_titles_director_id_index` (`director_id`),
  ADD KEY `directors_titles_title_id_index` (`title_id`);

--
-- Indexes for table `episodes`
--
ALTER TABLE `episodes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ep_s_title_unique` (`episode_number`,`season_number`,`title_id`),
  ADD KEY `episodes_season_id_index` (`season_id`),
  ADD KEY `episodes_episode_number_index` (`episode_number`),
  ADD KEY `episodes_season_number_index` (`season_number`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indexes for table `group_activity`
--
ALTER TABLE `group_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `images_local_unique` (`local`),
  ADD UNIQUE KEY `images_web_unique` (`web`),
  ADD KEY `images_title_id_index` (`title_id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `links_url_unique` (`url`),
  ADD KEY `links_approved_index` (`approved`),
  ADD KEY `links_title_id_index` (`title_id`),
  ADD KEY `links_season_index` (`season`),
  ADD KEY `links_episode_index` (`episode`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title_unique` (`title`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `options_name_unique` (`name`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_title_unique` (`title`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_link_id_index` (`link_id`),
  ADD KEY `reports_ip_address_index` (`ip_address`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `author_title_unique` (`title_id`,`author`),
  ADD KEY `reviews_title_id_index` (`title_id`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tile_number_unique` (`title_id`,`number`),
  ADD KEY `seasons_title_id_index` (`title_id`),
  ADD KEY `seasons_title_tmdb_id_index` (`title_tmdb_id`),
  ADD KEY `seasons_title_imdb_id_index` (`title_imdb_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social`
--
ALTER TABLE `social`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `service_user_identifier` (`service_user_identifier`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `titles_imdb_id_unique` (`imdb_id`),
  ADD UNIQUE KEY `titles_tmdb_id_type_unique` (`tmdb_id`,`type`),
  ADD KEY `titles_mc_num_of_votes_index` (`mc_num_of_votes`),
  ADD KEY `titles_created_at_index` (`created_at`),
  ADD KEY `titles_release_date_index` (`release_date`),
  ADD KEY `titles_title_index` (`title`),
  ADD KEY `titles_mc_user_score_index` (`mc_user_score`),
  ADD KEY `titles_tmdb_popularity_index` (`tmdb_popularity`),
  ADD KEY `titles_temp_id_index` (`temp_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_activation_code_index` (`activation_code`),
  ADD KEY `users_reset_password_code_index` (`reset_password_code`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `users_titles`
--
ALTER TABLE `users_titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_u_w_unique` (`title_id`,`user_id`,`watchlist`),
  ADD UNIQUE KEY `t_u_f_unique` (`title_id`,`user_id`,`favorite`),
  ADD KEY `users_titles_user_id_index` (`user_id`),
  ADD KEY `users_titles_title_id_index` (`title_id`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `writers`
--
ALTER TABLE `writers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `writers_name_unique` (`name`);

--
-- Indexes for table `writers_titles`
--
ALTER TABLE `writers_titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `writer_title_unique` (`writer_id`,`title_id`),
  ADD KEY `writers_titles_writer_id_index` (`writer_id`),
  ADD KEY `writers_titles_title_id_index` (`title_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actors`
--
ALTER TABLE `actors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `actors_titles`
--
ALTER TABLE `actors_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categorizables`
--
ALTER TABLE `categorizables`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=469;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `directors_titles`
--
ALTER TABLE `directors_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `episodes`
--
ALTER TABLE `episodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_activity`
--
ALTER TABLE `group_activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `social`
--
ALTER TABLE `social`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_titles`
--
ALTER TABLE `users_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `writers`
--
ALTER TABLE `writers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `writers_titles`
--
ALTER TABLE `writers_titles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
